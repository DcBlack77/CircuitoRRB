<?php $__env->startSection('content'); ?>


<div class="row">
    <div class="galeria" id="galeria">
        <div style="height:80px;"></div>
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <h2 class="titulo">Galerias</h2>
            <div class="titulo"style="height:30px;">
                <input style="color:black; text-align:center;" class="" type="text" id="search" placeholder="Buscar Galeria" autofocus />
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <div class="container">
                <?php $__currentLoopData = $galerias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $galeria): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="row">
                        <a href="<?php echo e(url('galeria/'.$galeria->slug)); ?>">
                            <div class="">
                                <h2 class="titulo" style="margin-top:1.5em; color:white;"><?php echo e($galeria->nombre); ?></h2>
                            </div>
                        </a>
                        <?php $__currentLoopData = $galeria->imagenes->take(6); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $imagene): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div style="margin-top:1.5em;" class="col-md-2 col-sm-3 col-xs-4">
                                <img class="img img-responsive img-thumbnail" height="170px" width="170px" src="<?php echo e(url('public/archivos/galeria/' . $imagene->archivo)); ?>" alt="<?php echo e($imagene->leyenda); ?>" width='100%' />
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="" style="height:65px; background-color: #000"></div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('pagina::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>