<link rel="stylesheet" type="text/css" href="<?php echo e(url('public/css/prettyPhoto.css')); ?>">
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="unoticias" id="unoticias">
        <div style="height:80px;"></div>
        <div class="col-md-12 col-sm-10 col-xs-10 col-lg-12">
            <div class="">
                <?php $i=1; ?>
                <?php $__currentLoopData = $noticias->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $noticia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                    $imagen = clone $imagenes;
                    $imagen = $imagen->where('noticias_id', $noticia->id)->get();
                    if (!$imagen) {
                        $imagen = new stdClass();
                        $imagen->leyenda = '';
                        $imagen->archivo = 'gob.png';
                    }
                    ?>
                    <?php if($i == 1 ): ?>
                    <?php endif; ?>
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <h3 class=" titulo-noticia"><?php echo $noticia->titulo; ?></h3>
                        <div class="row">
                            <div class="noticias card">
                                <div class="row">
                                    <div class="col-md-12 col-sm-10 col-xs-10 col-lg-12 noti-top"></div>
                                    <div class="col-md-1"></div>
                                </div>
                                <div class="texto-resumen" style="text-align:justify">
                                    <p style="margin-left:5px;margin-right:5px;"><?php echo $noticia->contenido_html; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4" style="scroll-behavior: smooth; margin-top: 75px;">
                        <div class="panel panel-warning" style="border-radius: 0px;">
                            <div class="panel-heading" style="border-radius: 0px;">
                                <h3 class="panel-title text-center"> Galeria Fotogr&aacute;fica</h3>
                            </div>
                            <div class="panel-body" style="background-color: rgb(0, 0, 0);">
                                <ul class="gallery clearfix" style="list-style:none">
                                    <?php $num = 0;?>
                                    <?php $__currentLoopData = $imagen; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li style="margin: 0px 1px 4px 0; list-style:none">
                                            <a href="<?php echo e(url('public/archivos/noticias/'.$image->archivo)); ?>" data-lightbox="roadtrip" title="<?php echo e($noticia->titulo); ?>">
                                                <img class="img-responsive" style="width:100%" src="<?php echo e(url('public/archivos/noticias/'.$image->archivo)); ?>" alt="<?php echo e($image->leyenda); ?>">
                                            </a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php if($noticia->audio === true): ?>
                        <div class="col-xs-12">
                            <audio style="width:80%;" src="<?php echo e(url('public/archivos/noticias/audio/'.$noticia->audio)); ?>" controls autoplay loop>

                            </audio>
                        </div>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('js'); ?>
    <script>
        lightbox.option({
          'resizeDuration': 200,
          'wrapAround': true
        })
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('pagina::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>