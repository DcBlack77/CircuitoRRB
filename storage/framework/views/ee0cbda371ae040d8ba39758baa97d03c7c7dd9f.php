<div id="programacion">
	<div class="row">
		<div class="titulo">
			<h2 style="color:white; margin-top:1.5em; margin-bottom:1em;">Programas</h2>
		</div>
		<div class="row col-md-12">
			<div id="carousel-programas" class="owl-carousel owl-theme">
				<?php $num = 0;
				?>
				<?php $__currentLoopData = $programaciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $programa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php
						if($num == 0 ){
							echo '<div class="item">';
						}else{
							echo '<div class="item">';
						}
						$num ++


					?>
						<div class="">
							<h4 class="modal-title contenedor-text"><?php echo e($controller->dias[$key]); ?></h4>
							<div class="modal-body">
								<?php $__currentLoopData = $programa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="col-md-3">
									<div class="contenedor" sytle="margin-right:30px; margin-left:30px;">
										<div class="ih-item square effect13 left_to_right col-md-3" sytle="margin-right:30px; margin-left:30px;">
											<a href="#programas">
											<div class="img" >
												<img style="" src="<?php echo e(url('public/img/logos-programas/'.$value['url'])); ?>" alt="img">
											</div>
											<div class="info">
												<h3><?php echo e($value['titulo']); ?></h3>
												<p><?php echo e($value['locutor']); ?></p>
												<p style="font: bold;">Inicio: <?php echo e($value['hora_ini']); ?> | Fin: <?php echo e($value['hora_fin']); ?> </p>
											</div>
											</a>
										</div>
									</div>
								</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</div>
						</div>
					</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		</div>

	</div>
</div>
