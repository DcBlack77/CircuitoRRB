<?php $__env->startSection('content'); ?>
<div class="" style="height:65px; background-color: #000"></div>
<!-- Carousel principal -->
<div id="carousel" class="owl-carousel hidden-xs">
	<?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="item">
			<img src="<?php echo e(url('public/img/banners/'. $banner['archivo'] )); ?>">
		</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php 
        //dd($banner);
     ?>
	<?php for($i=1; $i <= 7 ; $i++): ?>
	<?php endfor; ?>
</div>

<!-- NOTICIAS -->
<div class="row">
	<div class="unoticias" id="unoticias">
		<div class="col-md-12 col-sm-10 co-xs-10 col-lg-12">
			<h2 class="titulo" style="padding-bottom:2em;">NOTICIAS</h2>
			<div class="col-md-4 col-sm-8 col-xs-8 col-lg-4">
				<div class="row twitter">
					<div class="blog-item">
						<div class="blog-item-wrapper wow fadeIn" data-wow-delay="0.3s" >
							<div class="blog-item-img" style="margin-left:60px;">
								<a class="twitter-timeline" data-width="550" data-height="1180" data-theme="dark" data-link-color="#FAB81E" href="https://twitter.com/RRB1011FM">Tweets by RRB1011FM</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-8 col-sm-8 col-xs-8 col-lg-8">
				<h2>Últimas Noticias</h2>
				<div class="row">
					<div class="noticias">
						<div class="col-md-12 col-sm-8 col-xs-8 col-lg-12">
							<?php $i=1; ?>
							<?php $__currentLoopData = $noticias->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $noticia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($i == 1 ): ?>
								<?php endif; ?>
								<article class="articulo-noticias card">
									<div class="row">
                                        <div class="col-md-3 col-sm-8 col-xs-8 col-lg-3" style="margin-left:40px;">
                                            <a class="" href="<?php echo e(url('noticias/'.$noticia->slug)); ?>">
                                                <img class="img img-responsive img-thumbnail" height="250px" width="250px" src="<?php echo e(url('public/archivos/noticias/'.$noticia->imagenes[0]->archivo)); ?>" alt="<?php echo e($noticia->imagenes[0]->leyenda); ?>" height="100%">
                                            </a>
                                        </div>
                                        <div class=" col-md-1 col-sm-1 col-xs-1 col-lg-1" style="margin-left: 10px;">
                                            <div class="col-md-12 col-sm-1 col-xs-1 col-lg-12">
                                                <p class="fecha-dia"><?php echo e(str_pad($noticia->published_at->day, 2, '0', STR_PAD_LEFT)); ?></p>
                                            </div>
                                            <div class="col-md-12 col-sm-1 col-xs-1 col-lg-12">
                                                <p class="fecha-mes"> <?php echo e($controller->meses[$noticia->published_at->month]); ?></p>
                                            </div>
                                            <div class="col-md-12 col-sm-1 col-xs-1 col-lg-12">
                                                <p class="fecha-ano"><?php echo e($noticia->published_at->year); ?></p>
                                            </div>
                                            <div class="col-md-12 col-sm-10 col-xs-10 col-lg-12 noti-top"></div>
                                        </div>
                                        <div class="col-md-7 col-sm-8 col-xs-8 col-lg-7" style="margin-top: -40px; width: 420px; margin-left: 20px;">
                                            <div class="">
                                                <h3 class="titulo"><?php echo e($noticia->titulo); ?></h3>
                                            </div>
                                            <div class="contenido">
                                                <p style="font-size:14px;"><?php echo str_replace("\n", '<br/>', $controller->limit_text($noticia->resumen,60)); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="margin-bottom:25px;">
                <a href="noticias" target="_blank">
                    <h4 class="titulo"> Más Noticias</h4>
                </a>
            </div>
        </div>
    </div>
</div>



<!-- DEBATE -->
<div class="row" style="z-index:-1;">
    <?php if($debates == true): ?>
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <section id="debate" class="">
                <div class="row">
                    <img class="img img-responsive" src="<?php echo e(url('public/archivos/debate/debate.jpg')); ?>" data-original="" style="width: 100%; height: auto; display: inline; margin-top:0 auto;">
                </div>
                
                </div>
            </section>
        </div>
    <?php endif; ?>
</div>

<!-- PROGRAMAS -->
<div class="row" style="z-index:100;">
    <div id="programas" class="programas">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <div class="tabla-programas" >
                <?php echo $__env->make('pagina::partials.programas', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
    </div>
</div>

<!-- EVENTOS -->
<div class="row">
    <div id="eventos" class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
        <div class="row">
            <div class="titulo">
				<h2 style="color:white; margin-top:1.5em;">Eventos</h2>
			</div>
            <?php $__currentLoopData = $eventos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $evento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
                    <?php $__currentLoopData = $evento->imagenes->take(1); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $imagene): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="">
                            <img class="img img-responsive" src="<?php echo e(url('public/archivos/eventos/' .$imagene->archivo)); ?>" alt="<?php echo e($imagene->leyenda); ?>" width="100%">
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <div class="carousel-caption">
                        <h2><?php echo e($evento->lugar); ?></h2>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>

<!-- EFEMERIDES -->
<div class="row" id="efemerides">
    <div class="container">
        <div class="titulo">
            <h2 style="color:white; margin-top:1.5em;">Efemérides</h2>
        </div>
        <div class="row">
            <?php $__currentLoopData = $efemerides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $efemeride): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-3 col-sm-12">
                    <div class="planes popup-gallery">
                        <a href="<?php echo e(url('public/archivos/efemerides/'.$efemeride['url'])); ?>" title="<?php echo e($efemeride->titulo); ?>">
                            <img class="img img-responsive" alt="<?php echo e($efemeride->resumen); ?>" src="<?php echo e(url('public/archivos/efemerides/'.$efemeride['url'])); ?>" data-original="<?php echo e(url('public/archivos/efemerides/'.$efemeride['url'])); ?>" style="min-width: 263px; max-height: 262px !important; display: block; height: auto;">
                        </a>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>

<!-- GALERIA -->
<div class="row">
    <div id="galeria" class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
        <div class="container">
        <?php $__currentLoopData = $galerias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $galeria): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div class="row">
				<a href="<?php echo e(url('galeria/'.$galeria->slug)); ?>">
					<div class="">
	                    <h2 class="titulo" style="margin-top:1.5em; color:white;"><?php echo e($galeria->nombre); ?></h2>
	                </div>
	                <?php $__currentLoopData = $galeria->imagenes->take(6); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $imagene): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                    <div class="col-md-2 col-sm-3 col-xs-4">
	                        <img class="img img-responsive img-thumbnail" height="170px" width="170px" src="<?php echo e(url('public/archivos/galeria/' . $imagene->archivo)); ?>" alt="<?php echo e($imagene->leyenda); ?>" width='100%' />
	                    </div>
	                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</a>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>

<!-- AUDIOS -->


<!-- VIDEOTECA -->
<div class="row">
    <div id="videos" class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
        <div id="videoteca">
            <div class="titulo">
                <h2 style="color:white;margin-top:1.5em;">Vídeos</h2>
            </div>
            <div class="row">
                <?php $__currentLoopData = $videos->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-5 col-sm-12 col-xs-12 col-lg-5" style="margin-left:auto;margin-right: 85px; margin-bottom:25px;">
                        <div class="videoscs">
                            <?php echo $video->get_iframe(); ?>

                            <div class="carousel-caption">
                                <h2><?php echo e($video->titulo); ?></h2>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="margin-bottom:25px;">
                <a href="https://www.youtube.com/channel/UCbb9IJF38A3cxZKds_PDayQ/videos" target="_blank">
                    <h4 class="titulo"> Más Vídeos</h4>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- ORGANIZACION -->
<div class="row" id="organizacion">
    <?php if($organizaciones == true): ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <div class="resumen">
                    <div class="col-md-offset-2 col-md-8">
                        <div class="titulo">
                            <h2>Reseña Histórica</h2>
                        </div>
                        <div class="texto">
                            <?php echo $organizaciones->objetivo; ?>

                        </div>
                    </div>
                </div>
                <div class="resumen">
                    <div class="col-md-offset-1 col-md-4">
                        <div class="titulo">
                            <h2>Misión</h2>
                        </div>
                        <div class="texto">
                            <?php echo $organizaciones->mision; ?>

                        </div>
                    </div>
                </div>
                <div class="resumen">
                    <div class="col-md-offset-2 col-md-4">
                        <div class="titulo">
                            <h2>Visión</h2>
                        </div>
                        <div class="texto">
                            <?php echo $organizaciones->vision; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>


<!-- Realizado por la dirección de Informatica y Sistemas de la Gobernación del Estado Bolívar -->


<!-- Creditos: -->

<!-- Diseño: Maria Campora "camporamaria@gmail.com" -->

<!-- Desarrollador: Alejandro Mendez "alejmendez.87@gmail.com" -->

<!-- Desarrollador: Diego Cabrera "diegocabrera123@gmail.com" -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('pagina::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>