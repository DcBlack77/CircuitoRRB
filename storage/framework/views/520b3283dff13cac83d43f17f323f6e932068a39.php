<?php $__env->startSection('content'); ?>
    <div style="height:80px;"></div>
<div class="col-md-12 col-sm-10 col-xs-10 col-lg-12">
    <?php $i=1; ?>
    <?php $__currentLoopData = $galerias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $galeria): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($i == 1 ): ?>
        <?php endif; ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h3 class=" titulo-noticia"><?php echo $galeria->titulo; ?></h3>
            <div class="col-xs-12 col-md-12" style="scroll-behavior: smooth; margin-top: 75px;">
                <div class="panel panel-warning" style="border-radius: 0px;">
                    <div class="panel-heading" style="border-radius: 0px;">
                        <h3 class="panel-title text-center"> Galeria Fotogr&aacute;fica</h3>
                    </div>
                    <div class="panel-body" style="background-color: rgb(0, 0, 0);">
                        <ul class="gallery clearfix" style="list-style:none">
                            <?php $num = 0;?>
                            <?php $__currentLoopData = $galeria->imagenes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $imagene): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class"col-md-4">
                                    <a href="<?php echo e(url('public/archivos/galeria/' . $imagene->archivo)); ?>" data-lightbox="roadtrip" title="<?php echo e($galeria->titulo); ?>">
                                        <img class="img-responsive" style="width:100%" src="<?php echo e(url('public/archivos/galeria/' . $imagene->archivo)); ?>" alt="<?php echo e($imagene->leyenda); ?>">
                                    </a>
                                </div>
                                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('pagina::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>