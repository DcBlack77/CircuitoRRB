<?php $__env->startSection('content'); ?>


<div class="row">
    <div class="audios" id="audio">
        <div style="height:80px;"></div>
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <h2 class="titulo">Audios</h2>
            <div class="titulo"style="height:30px;">
                <input style="color:black; text-align:center;" class="" type="text" id="search" placeholder="Buscar Audio" autofocus />
            </div>
            <div class="container">
                <?php $__currentLoopData = $audios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $audio): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="contenedor">
                        <div class="col-md-offset-2 col-md-8">
                            <div class="row">
                                <a href="<?php echo e(url('audio/'.$audio->slug)); ?>">
                                    <div class="noticias card ho">
                                        <div class="col-md-12">
                                            <h3 class=" titulo-noticia"><?php echo $audio->titulo; ?></h3>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="" style="margin-top:1em;">
                                                <center>
                                                    <audio style="width:80%;"  src="<?php echo e(url('public/audio/'. $audio->archivo)); ?>" controls="controls" preload="">

                                                    </audio>
                                                </center>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="margin-top:1em;">
                                            <div class="texto-resumen" style="text-align:justify">
                                                <center><p><?php echo str_replace("\n", '<br/>', $controller->limit_text($audio->descripcion,90)); ?></p></center>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>
<div class="" style="height:65px; background-color: #000"></div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('pagina::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>