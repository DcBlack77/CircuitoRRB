<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix').'/banner', 'namespace' => 'App\\Modules\Banner\Http\Controllers'], function()
{
    Route::get('/',                         'BannerController@index');
     Route::get('buscar/{id}',              'BannerController@buscar');

     Route::post('guardar',                 'BannerController@guardar');
     Route::put('guardar/{id}',             'BannerController@guardar');

     Route::delete('eliminar/{id}',         'BannerController@eliminar');
     Route::post('restaurar/{id}',          'BannerController@restaurar');
     Route::delete('destruir/{id}',         'BannerController@destruir');

     Route::get('datatable',                'BannerController@datatable');
     Route::post('subir',                   'BannerController@subir');
});
