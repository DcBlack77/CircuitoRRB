<?php

namespace App\Modules\Banner\Http\Requests;

use App\Http\Requests\Request;

class BannerRequest extends Request {
	protected $reglasArr = [
		'categorias_id' => ['required', 'integer'],
		'archivo' => ['required'],
		'activo' => ['required'],
	];
}
