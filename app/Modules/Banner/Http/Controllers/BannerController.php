<?php

namespace App\Modules\Banner\Http\Controllers;

//controlador Padre
use App\Modules\Banner\Http\Controllers\Controller;


//Dependencias
use DB;
use Validator;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

//Request
use App\Modules\Banner\Http\Requests\BannerRequest;

//Modelos
use App\Modules\Banner\Models\Banner;
use App\Modules\Noticias\Models\Categorias;

class BannerController extends Controller
{
    protected $titulo = 'Banner';

    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker',
        'file-upload',
        'jcrop',
    ];

    public $js = [
        'Banner'
    ];

    public function index() {
        return $this->view('banner::Banner');
    }

    public function buscar(Request $request, $id = 0){
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')){
            $Banner = Banner::withTrashed()->find($id);
        }else{
            $Banner = Banner::find($id);
        }

        if ($Banner){
            $url = $this->ruta();
            $url = substr($url, 0, strlen($url) - 7);
            $imgArray = [];

                $id_archivo = str_replace('/', '-', $Banner->archivo);
                $name = substr($id_archivo, strrpos($id_archivo, '/') + 1);
                $imgArray[] = [
                    'id' => $id_archivo,
                    'name' => $name,
                    'url' => url('public/img/banners/' . $Banner->archivo),
                    'thumbnailUrl' => url('public/img/banners/' . $Banner->archivo),
                    'deleteType' => 'DELETE',
                    'deleteUrl' => url($url . '/eliminarimagen/' . $id_archivo),
                    'data' => [
                        'cordenadas' => [],
                        'leyenda' => '',
                        'descripcion' => ''
                    ]
                ];
            return array_merge($Banner->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar'),
                'files' => $imgArray,
            ]);
        }

        return trans('controller.nobuscar');
    }

    protected function data($request) {
        $data = $request->all();


        $archivos = json_decode($request->archivo);

        foreach ($archivos as $archivo => $dato) {
            if (!preg_match("/^(\d{4})\-(\d{2})\-([0-9a-z\.]+)\.(jpe?g|png)$/i", $archivo)) {
                continue;
            }
            $archivo = str_replace('-', '/', $archivo);
        }

        $data ['archivo'] = $archivo;

        return $data;
    }

    public function guardar(BannerRequest $request, $id = 0){
        DB::beginTransaction();
        try{

            $data = $this->data($request);

            if ($id === 0){
                Banner::create($data);
            }else{
                Banner::find($id)->update($data);
            }
        }catch(Exception $e){
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return ['s' => 's', 'msj' => trans('controller.incluir')];
    }

    public function eliminar(Request $request, $id = 0){
        try{
            Banner::destroy($id);
        }catch(Exception $e){
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0) {
        try {
            Banner::withTrashed()->find($id)->restore();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0) {
        try {
            Banner::withTrashed()->find($id)->forceDelete();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request){
        $sql = Banner::select([
            'banner.id as id',
            'categorias.nombre as categoria',
            'banner.archivo',
            'banner.activo'
        ])->leftJoin('categorias', 'banner.categorias_id', '=', 'categorias.id');

        if ($request->verSoloEliminados == 'true'){
            $sql->onlyTrashed();
        }elseif ($request->verEliminados == 'true'){
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->editColumn('archivo', '<img src="{{ url(\'public/img/banners/\'.$archivo)}}" width="100">')
            ->make(true);
    }


    public function categoria(){

        return  Categorias::pluck('nombre', 'id');
    }

    public function subir(Request $request) {
        $validator = Validator::make($request->all(), [
            'files.*' => ['required', 'mimes:jpeg,jpg,png'],
        ]);

        if ($validator->fails()) {
            return ['s' => 'n', 'msj' => 'Ocurrió un error el archivo debe ser jpg o png'];
        }

        $files = $request->file('files');
        $url = $this->ruta();
        $url = substr($url, 0, strlen($url) - 6);

        $rutaFecha = $this->getRuta();
        $ruta = public_path('img/banners/' . $rutaFecha);

        $respuesta = array(
            'files' => array(),
        );

        foreach ($files as $file) {
            do {
                $nombre_archivo = $this->random_string() . '.' . $file->getClientOriginalExtension();
            } while (is_file($ruta . $nombre_archivo));

            $id = str_replace('/', '-', $rutaFecha . $nombre_archivo);

            $respuesta['files'][] = [
                'id' => $id,
                'name' => $nombre_archivo,
                'size' => $file->getSize(),
                'type' => $file->getMimeType(),
                //'url' => url('imagen/small/' . $rutaFecha . $nombre_archivo),
                'url' => url('public/img/banners/' . $rutaFecha . $nombre_archivo),
                'thumbnailUrl' =>url('public/img/banners/' . $rutaFecha . $nombre_archivo),
                'deleteType' => 'DELETE',
                'deleteUrl' => url($url . '/eliminarimagen/' . $id),
                'data' => [
                    'cordenadas' => [],
                    'leyenda' => '',
                    'descripcion' => ''
                ]
            ];

            $mover = $file->move($ruta, $nombre_archivo);
        }

        return $respuesta;
    }

    protected function getRuta() {
        return date('Y') . '/' . date('m') . '/';
    }

    protected function random_string($length = 20) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }
}
