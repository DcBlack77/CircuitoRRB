<?php

namespace App\Modules\Banner\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'banner';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'app/Modules/Banner/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'app/Modules/Banner/Assets/css',
	];
}
