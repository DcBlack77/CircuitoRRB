<?php
$menu['noticias'] = [
    [
		'nombre'	=>	'Noticias',
		'direccion'	=>	'#Noticias',
		'icono'		=>	'fa fa-folder',
        'menu'      =>  [

            [
                'nombre'    => 'Noticas',
                'direccion' =>  'noticias',
                'icono'     =>  'fa fa-quote-right'
            ],
            [
                'nombre'    =>  'Categorias',
                'direccion' =>  'noticias/definiciones/categorias',
                'icono'     =>  'fa fa-pencil-square-o'
            ]
        ]
	]
];
 ?>
