<?php

namespace App\Modules\Noticias\Http\Requests;

use App\Http\Requests\Request;

class noticiasRequest extends Request {
    protected $reglasArr = [
        'titulo' 			=> ['required', 'min:3', 'max:250'],
        'slug'				=> ['required'],
        'contenido_html' 	=> ['required'],
        'resumen' 			=> ['required'],
        'published_at' 		=> [''],
    ];
}
