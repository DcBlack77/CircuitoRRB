<?php

namespace App\Modules\Noticias\Http\Requests;

use App\Http\Requests\Request;

class EstatusRequest extends Request {
	protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:50', 'unique:estatus,nombre']
	];
}
