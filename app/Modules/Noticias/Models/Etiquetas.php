<?php

namespace app\Modules\Noticias\Models;

use app\Modules\base\Models\Modelo;



class Etiquetas extends modelo
{
    protected $table = 'etiquetas';
    protected $fillable = ["nombre","slug"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Etiquetas'
    ],
    'slug' => [
        'type' => 'text',
        'label' => 'Slug',
        'placeholder' => 'Slug del Etiquetas'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

    }

    public function noticias()
    {
        return $this->belongsToMany('App\Modules\Noticas\Models\Noticias', 'noticia_etiqueta');
    }


}
