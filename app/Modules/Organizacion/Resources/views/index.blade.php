@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')


	@include('base::partials.ubicacion', ['ubicacion' => ['organizacion']])
	@include('base::partials.botonera')
	@include('base::partials.modal-busqueda', [
		'titulo' => 'Buscar organizacion.',
		'columnas' => [
			'mision'  => '33.3',
			'vision'  => '33.3',
			'valores' => '33.3',
		]
	])

@endsection

@include('organizacion::partials.misionconten')
