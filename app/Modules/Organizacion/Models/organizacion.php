<?php

namespace App\Modules\Organizacion\Models;

use App\Modules\Base\Models\Modelo;



class organizacion extends Modelo
{
    protected $table = 'organizacion';
    protected $fillable = ['mision', 'vision', 'objetivo'];
    protected $hidden = ['created_at', 'updated_at','deleted_at'];
    //"id",    "mision",    "vision",    "valores",    "objetivo"
    /*
    protected $campos = [
    'mision' => [
    'type' => 'textarea',
    'label' => '',
    'placeholder' => '',
    'class_cont' => ''
]
]
*/
}
