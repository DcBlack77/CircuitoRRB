<?php

namespace App\Modules\Organizacion\Http\Controllers;

use DB;
use Validator;
use App\Modules\Organizacion\Http\Controllers\Controller;

//Dependencias


use Yajra\Datatables\Datatables;


//Request
use App\Http\Requests\Request;
use App\Modules\Organizacion\Http\Requests\MisionRequest;
// modelo
use App\Modules\Organizacion\Models\organizacion;


class misionController extends Controller {
    protected $titulo = 'Misión';

    public $js =[
        'mision'
    ];
    public $css =[

    ];
    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
        'ckeditor',
        'jquery-ui',
        'jquery-ui-timepicker',
        'file-upload',
        'jcrop'
    ];


    /**
    * Display a listing of the resource.
    * @return Response
    */
    public function index()

    {
        //dd('omar'); 'Organizacion' => new Organizacion()
        return $this->view('organizacion::index');
    }

    public function buscar(Request $request, $id ){

        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')){

            $organizacion = organizacion::withTrashed()->find($id);

        }else{
            $organizacion = organizacion::find($id);
        }

        if ($organizacion) {
            return array_merge($organizacion->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function data($request){
        if ($this->puedePublicar() && $request->input(['valores']) != '') {

            $data=$request->all();

        }else {
            //$data=$request->except(['published_at']);

            $data['valores']= strip_tags($data ['valores']);
        }
        // $data['app_usuario_id'] = auth()->user()->id;
        // $data['slug'] = str_slug($data['titulo'],'-');

        return $data;
    }

    public function guardar(MisionRequest $request, $id = 0) {

        //
        //dd($request->all());

        try {
            if ($id === 0){
                //      dd($id);
                organizacion::create($request->all());
            }else{
                organizacion::find($id)->update($request->all());
            }
        } catch (organizacion $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.incluir')];
        //

    }

    public function eliminar(Request $request, $id=0){
        try{
            $rs=organizacion::destroy($id);
        }catch (Exception $e){
            return $e->errorInfo[2];
        }
        return ['s'=>'s', 'msj'=>trans('controller.eliminar')];
    }

    public function datatable() {
        $sql = organizacion::select([
            'organizacion.id','organizacion.mision', 'organizacion.vision', 'organizacion.objetivo',
        ]);

        return Datatables::of($sql)->setRowId('id')->make(true);
    }

}
