<?php

Route::group(['middleware' => 'web', 'prefix'=>Config::get('admin.prefix') ,'namespace' => 'App\\Modules\Organizacion\Http\Controllers'], function()
{

    Route::group(['prefix' => 'organizacion'], function() {
		Route::get('/', 				'misionController@index');
		Route::get('buscar/{id}', 		'misionController@buscar');
		Route::get('nuevo', 		    'misionController@nuevo');

		Route::get('cambiar/{id}', 		'misionController@cambiar');
		Route::post('guardar',			'misionController@guardar');
		Route::put('guardar/{id}', 		'misionController@guardar');

		Route::delete('eliminar/{id}', 	'misionController@eliminar');
		Route::post('restaurar/{id}', 	'misionController@restaurar');
		Route::delete('destruir/{id}', 	'misionController@destruir');

		Route::post('cambio', 			'misionController@cambio');
		Route::get('datatable', 		'misionController@datatable');
	});



});
