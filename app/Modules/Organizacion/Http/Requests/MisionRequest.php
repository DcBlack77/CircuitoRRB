<?php

namespace App\Modules\Organizacion\Http\Requests;

use App\Http\Requests\Request;

class MisionRequest extends Request {
    protected $reglasArr = [
        'mision'    =>  ['required', 'min:3', 'max:2500'],
        'vision'    =>  ['required', 'min:3', 'max:2500'],
        'objetivo'  =>  ['required', 'min:3', 'max:2500'],
    ];
}
