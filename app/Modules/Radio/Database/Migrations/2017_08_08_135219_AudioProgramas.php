<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AudioProgramas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audio_programas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 200);
            $table->string('url', 255);
            $table->integer('programas_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('programas_id')
                ->references('id')->on('programas')
                    ->onDelete('cascade')->onUpdate('cascade');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('audio_programas');
    }
}
