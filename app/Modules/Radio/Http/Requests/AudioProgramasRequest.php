<?php

namespace Modules\Radio\Http\Requests;

use App\Http\Requests\Request;

class AudioProgramasRequest extends Request {
    protected $reglasArr = [
		'titulo' => ['required', 'min:3', 'max:200'], 
		'url' => ['required', 'min:3', 'max:255'], 
		'programas_id' => ['required', 'integer']
	];
}