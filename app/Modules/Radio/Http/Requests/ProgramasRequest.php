<?php

namespace App\Modules\Radio\Http\Requests;

use App\Http\Requests\Request;

class ProgramasRequest extends Request {
    protected $reglasArr = [
		'titulo' => ['required', 'min:3', 'max:200'],
		'locutor' => ['required', 'min:3', 'max:100'],
		'dias' => ['required'],
		'hora_ini' => ['required', 'min:3', 'max:100'],
		'hora_fin' => ['required', 'min:3', 'max:100'],
		//'url' => ['min:3', 'max:255']
	];
}
