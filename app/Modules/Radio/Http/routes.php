<?php


Route::group(['middleware' => 'web', 'prefix' =>  Config::get('admin.prefix').'/radio', 'namespace' => 'App\\Modules\Radio\Http\Controllers'], function()
{
    Route::group(['prefix' => 'programa'], function() {
		Route::get('/', 				'ProgramasController@index');
		Route::get('buscar/{id}', 		'ProgramasController@buscar');

		Route::post('guardar',			'ProgramasController@guardar');
		Route::put('guardar/{id}', 		'ProgramasController@guardar');

		Route::delete('eliminar/{id}', 	'ProgramasController@eliminar');
		Route::post('restaurar/{id}', 	'ProgramasController@restaurar');
		Route::delete('destruir/{id}', 	'ProgramasController@destruir');

		Route::get('datatable', 		'ProgramasController@datatable');

    });
    Route::group(['prefix' => 'audioprograma'], function() {
		Route::get('/', 				'AudioProgramasController@index');
		Route::get('buscar/{id}', 		'AudioProgramasController@buscar');

		Route::post('guardar',			'AudioProgramasController@guardar');
		Route::put('guardar/{id}', 		'AudioProgramasController@guardar');

		Route::delete('eliminar/{id}', 	'AudioProgramasController@eliminar');
		Route::post('restaurar/{id}', 	'AudioProgramasController@restaurar');
		Route::delete('destruir/{id}', 	'AudioProgramasController@destruir');

		Route::get('datatable', 		'AudioProgramasController@datatable');

    });
});
