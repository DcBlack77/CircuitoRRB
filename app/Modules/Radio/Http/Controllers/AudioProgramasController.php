<?php

namespace App\Modules\Radio\Http\Controllers;

//Controlador Padre
use App\Modules\Radio\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
/* use App\Modules\Radio\Http\Requests\AudioProgramasRequest; */

//Modelos
use App\Modules\Radio\Models\AudioProgramas;

class AudioProgramasController extends Controller
{
    protected $titulo = 'Audio Programas';

    public $js = [
        'AudioProgramas'
    ];
    
    public $css = [
        'AudioProgramas'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('radio::AudioProgramas', [
            'AudioProgramas' => new AudioProgramas()
        ]);
    }

    public function nuevo()
    {
        $AudioProgramas = new AudioProgramas();
        return $this->view('radio::AudioProgramas', [
            'layouts' => 'base::layouts.popup',
            'AudioProgramas' => $AudioProgramas
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $AudioProgramas = AudioProgramas::find($id);
        return $this->view('radio::AudioProgramas', [
            'layouts' => 'base::layouts.popup',
            'AudioProgramas' => $AudioProgramas
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $AudioProgramas = AudioProgramas::withTrashed()->find($id);
        } else {
            $AudioProgramas = AudioProgramas::find($id);
        }

        if ($AudioProgramas) {
            return array_merge($AudioProgramas->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(Request $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $AudioProgramas = $id == 0 ? new AudioProgramas() : AudioProgramas::find($id);

            $AudioProgramas->fill($request->all());
            $AudioProgramas->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $AudioProgramas->id,
            'texto' => $AudioProgramas->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            AudioProgramas::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            AudioProgramas::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            AudioProgramas::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = AudioProgramas::select([
            'id', 'titulo', 'url', 'programas_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}