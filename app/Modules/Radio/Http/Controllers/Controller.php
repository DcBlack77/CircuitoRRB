<?php

namespace App\Modules\Radio\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'base';
	// public $autenticar = false;
	protected $titulo = 'Radio';
	// public $prefijo_ruta = 'backend';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'app/Modules/Radio/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'app/Modules/Radio/Assets/css',
	];

}
