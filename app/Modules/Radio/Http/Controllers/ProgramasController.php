<?php

namespace App\Modules\Radio\Http\Controllers;

//Controlador Padre
use App\Modules\Radio\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use URL;
//Request
use App\Modules\Radio\Http\Requests\ProgramasRequest;

//Modelos
use App\Modules\Radio\Models\Programas;

class ProgramasController extends Controller
{
    protected $titulo = 'Programas';

    public $js = [
        'Programas'
    ];

    public $css = [
        'Programas'
    ];

    public $librerias = [
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker',
        'bootstrap-select'
    ];

    public $dias = [
        "Lunes",
        "Martes",
        "Miercoles",
        "Jueves",
        "Viernes",
        "Sabado",
        "Domingo"
    ];


    public function dias() {
        return $this->dias;
    }

    public function index()
    {
        return $this->view('radio::Programas', [
            'Programas' => new Programas()
        ]);
    }

    public function nuevo()
    {
        $Programas = new Programas();
        return $this->view('radio::Programas', [
            'layouts' => 'base::layouts.popup',
            'Programas' => $Programas
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Programas = Programas::find($id);
        return $this->view('radio::Programas', [
            'layouts' => 'base::layouts.popup',
            'Programas' => $Programas
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Programas = Programas::withTrashed()->find($id);
        } else {
            $Programas = Programas::find($id);
        }

        if ($Programas) {
            $dias = [];
            $_dias = explode( ',', $Programas->dias );
            foreach ($_dias as $key => $dia) {
                $dias[] = intval($dia);
            }
            // dd($_dias);
            $Programas->url = URL::to("public/img/logos-programas/" . $Programas->url);
            return array_merge($Programas->toArray(), [
                'dias' => $dias,
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    protected function random_string($length = 10) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }
    public function guardar(ProgramasRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $data = $request->all();
            //$foto;

            if($file = $request->file('foto')){
                $data['url'] = $this->random_string() . '.' . $file->getClientOriginalExtension();

                $path = public_path('img/logos-programas/');
                $file->move($path, $data['url']);
                chmod($path . $data['url'], 0777);
            }

            //$data = $request->all();

            //$data['url']  = $foto;

            $data['dias'] = implode(",", $data['dias']);
            $Programas = $id == 0 ? new Programas() : Programas::find($id);

            $Programas->fill($data);
            $Programas->save();

        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Programas->id,
            'texto' => $Programas->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Programas::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Programas::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Programas::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Programas::select([
            'id', 'titulo', 'locutor', 'url', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}
