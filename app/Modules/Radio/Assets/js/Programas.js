var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();
			$("#foto").prop("src", "../../public/img/usuarios/user.png");
		},
		'buscar' : function(r){
			$('#btn1').click();
			$("#foto").prop("src", r.url);
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{data:"titulo",name:"titulo"}
		]
	});

	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	//fotos
	$("#upload_link").on('click', function(e) {
		e.preventDefault();
		$("#upload:hidden").trigger('click');
	});

	$('#hora_ini', $form).timepicker();
	$('#hora_fin', $form).timepicker();
});
