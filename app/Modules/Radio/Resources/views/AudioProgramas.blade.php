@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Audio Programas']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar AudioProgramas.',
        'columnas' => [
            'Titulo' => '33.333333333333',
		'Url' => '33.333333333333',
		'Programas' => '33.333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $AudioProgramas->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection