<?php

$menu['radio'] = [
	[
		'nombre' 	=> 'Radio',
		'direccion' => '#Radio',
		'icono' 	=> 'fa fa-gear',
		'menu' 		=> [
			[
				'nombre' 	=> 'Programas',
				'direccion' => 'radio/programa',
				'icono' 	=> 'fa fa-user'
			],
			[
				'nombre' 	=> 'Audios Programas ',
				'direccion' => 'radio/audioprograma',
				'icono' 	=> 'fa fa-users'
			]
		]
	]
];
