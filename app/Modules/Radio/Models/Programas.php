<?php

namespace App\Modules\Radio\Models;

use App\Modules\base\Models\Modelo;



class Programas extends modelo
{
    protected $table = 'programas';
    protected $fillable = ["titulo","dias","hora_ini","hora_fin","locutor","url"];
    protected $campos = [
    'titulo' => [
        'type' => 'text',
        'label' => 'Titulo',
        'placeholder' => 'Titulo del Programas'
    ],
    'dia' => [
        'type' => 'text',
        'label' => 'Dia',
        'placeholder' => 'Dia del Programas'
    ],
    'hora' => [
        'type' => 'text',
        'label' => 'Hora',
        'placeholder' => 'Hora del Programas'
    ],
    'locutor' => [
        'type' => 'text',
        'label' => 'Locutor',
        'placeholder' => 'Locutor del Programas'
    ],
    'url' => [
        'type' => 'text',
        'label' => 'Url',
        'placeholder' => 'Url del Programas'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

    }

    public function audio_programas()
    {
        return $this->hasMany('App\ModulesRadio\Models\AudioProgramas');
    }


}
