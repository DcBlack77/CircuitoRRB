<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Eventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('slug')->unique();
            $table->string('lugar');
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('eventos_img', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('eventos_id')->unsigned();
			$table->string('archivo', 200);
			$table->string('descripcion', 200);
			$table->string('leyenda', 200);
			$table->string('tamano', 12);

			$table->foreign('eventos_id')
				->references('id')->on('eventos')
				->onDelete('cascade')->onUpdate('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eventos_img');
        Schema::drop('eventos');
    }
}
