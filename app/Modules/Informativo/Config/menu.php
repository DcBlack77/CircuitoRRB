<?php
    $menu['informativo']    =   [
        [
            'nombre'    =>  'Informativo',
            'direccion' =>  '#Informativo',
            'icono'     =>  'fa fa-info',
            'menu'      =>  [
                [
                    'nombre'    =>  'Debate',
                    'direccion' =>  'informativo/debate',
                    'icono'     =>  'fa fa-gavel'
                ],
                [
                    'nombre'    =>  'Efemerides',
                    'direccion' =>  'informativo/efemerides',
                    'icono'     =>  'fa fa-glass'
                ],
                [
                    'nombre'    =>  'Eventos',
                    'direccion' =>  'informativo/eventos',
                    'icono'     =>  'fa fa-star'
                ],
            ]
        ]
    ];
?>
