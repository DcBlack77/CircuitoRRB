<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix').'/informativo', 'namespace' => 'App\\Modules\Informativo\Http\Controllers'], function()
{
    Route::get('/',                     'InformativoController@index');

    Route::group(['midlleware'=>'web', 'prefix' => '/debate'],function(){
        Route::get('/',                 'DebateController@index');
        Route::get('/datatable',        'DebateController@datatable');
        Route::post('/guardar',         'DebateController@guardar');
        Route::get('buscar/{id}',       'DebateController@buscar');
    });
    Route::group(['midlleware'=>'web', 'prefix' => '/efemerides'],function(){

        Route::get('/',                     'EfemeridesController@index');
        Route::get('nuevo',                 'EfemeridesController@nuevo');
        Route::get('cambiar/{id}',          'EfemeridesController@cambiar');

        Route::get('buscar/{id}',           'EfemeridesController@buscar');

        Route::post('guardar',              'EfemeridesController@guardar');
        Route::put('guardar/{id}',          'EfemeridesController@guardar');

        Route::delete('eliminar/{id}',      'EfemeridesController@eliminar');
        Route::post('restaurar/{id}',       'EfemeridesController@restaurar');
        Route::delete('destruir/{id}',      'EfemeridesController@destruir');

        Route::get('datatable',             'EfemeridesController@datatable');
        Route::post('subir',                'EfemeridesController@subir');
    });


    Route::group(['prefix' => 'eventos'], function() {
        Route::get('/',                     'EventosController@index');
        Route::get('nuevo',                 'EventosController@nuevo');
        Route::get('cambiar/{id}',          'EventosController@cambiar');

        Route::get('buscar/{id}',           'EventosController@buscar');

        Route::post('guardar',              'EventosController@guardar');
        Route::put('guardar/{id}',          'EventosController@guardar');

        Route::delete('eliminar/{id}',      'EventosController@eliminar');
        Route::post('restaurar/{id}',       'EventosController@restaurar');
        Route::delete('destruir/{id}',      'EventosController@destruir');

        Route::get('datatable',             'EventosController@datatable');
        Route::post('subir',                'EventosController@subir');
    });

//{{route}}
});
