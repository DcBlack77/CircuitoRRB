<?php

namespace App\Modules\Informativo\Http\Controllers;

//Controlador Padre
use App\Modules\Informativo\Http\Controllers\Controller;

//Dependencias
use DB;
use Validator;
use Image;
use Yajra\Datatables\Datatables;
use FTP;
use Carbon\Carbon;
use Illuminate\Http\Request;
use URL;

//Request
use App\Modules\Informativo\Http\Requests\EfemeridesRequest;

//Modelos
use App\Modules\Informativo\Models\Efemerides;
use App\Modules\Informativo\Models\EfemeridesImg;


class EfemeridesController extends Controller
{
    protected $titulo = 'Efemerides';

    public $js = [
        'Efemerides'
    ];

    public $css = [
        'Efemerides'
    ];

    public $librerias = [
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker'
    ];

    public function index()
    {
        return $this->view('informativo::Efemerides', [
            'Efemerides' => new Efemerides()
        ]);
    }

    public function nuevo()
    {
        $Efemerides = new Efemerides();
        return $this->view('informativo::Efemerides', [
            'layouts' => 'base::layouts.popup',
            'Efemerides' => $Efemerides
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Efemerides = Efemerides::find($id);
        return $this->view('informativo::Efemerides', [
            'layouts' => 'base::layouts.popup',
            'Efemerides' => $Efemerides
        ]);
    }

    public function buscar(Request $request, $id =0){
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Efemerides = Efemerides::withTrashed()->find($id);
        } else {
            $Efemerides = Efemerides::find($id);
        }

        $path = public_path('archivos/efemerides/');

        if ($Efemerides) {
            $Efemerides->url = URL::to('archivos/efemerides/'. $Efemerides->url);
            return array_merge($Efemerides->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }


    public function data($request){
        if ($this->puedePublicar() && $request->input(['published_at']) != '') {

            $data=$request->all();

        }else {
            $data=$request->except(['published_at']);
        }
        // $data['slug'] = str_slug($data['titulo'],'-');

        return $data;
    }

    public function guardar(EfemeridesRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $data = $request->all();

            if($file = $request->file('foto')){
                $data['url'] = $this->random_string() . '.' . $file->getClientOriginalExtension();

                $path = public_path('archivos/efemerides/');
                $file->move($path, $data['url']);
                chmod($path . $data['url'], 0777);
            }

            $Efemerides = $id == 0 ? new Efemerides() : Efemerides::find($id);

            $Efemerides->fill($data);
            $Efemerides->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Efemerides->id,
            'texto' => $Efemerides->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    protected function getRuta() {
        return date('Y') . '/' . date('m') . '/';
    }

    protected function random_string($length = 10) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    public function puedePublicar(){
        return strtolower(auth()->user()->super) === 's' || $this->permisologia('publicar');
    }

    public function eliminarImagen(Request $request, $id=0){
        $id = str_replace('-', '/', $id);
		try {
			// \File::delete(public_path('img/noticias/' . $id));
			$rs = EfemeridesImg::where('archivo', $id)->delete();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Efemerides::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Efemerides::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Efemerides::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Efemerides::select([
            'id', 'titulo', 'resumen','url','slug', 'published_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}
