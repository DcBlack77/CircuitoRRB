<?php

namespace App\Modules\Informativo\Http\Controllers;

//Controlador Padre
use App\Modules\Informativo\Http\Controllers\Controller;

//Dependencias
use DB;
use Validator;
use Image;
use Yajra\Datatables\Datatables;
use FTP;
use Carbon\Carbon;
use Illuminate\Http\Request;

//Request
use App\Modules\Informativo\Http\Requests\EventosRequest;

//Modelos
use App\Modules\Informativo\Models\Eventos;
use App\Modules\Informativo\Models\EventosImg;

class EventosController extends Controller
{
    protected $titulo = 'Eventos';

    public $js = [
        'Eventos'
    ];

    public $css = [
        'Eventos'
    ];

    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
        'ckeditor',
        'jquery-ui',
        'jquery-ui-timepicker',
        'file-upload',
        'jcrop'
    ];

    public function index()
    {
        return $this->view('informativo::Eventos', [
            'Eventos' => new Eventos()
        ]);
    }

    public function nuevo()
    {
        $Eventos = new Eventos();
        return $this->view('informativo::Eventos', [
            'layouts' => 'base::layouts.popup',
            'Eventos' => $Eventos
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Eventos = Eventos::find($id);
        return $this->view('informativo::Eventos', [
            'layouts' => 'base::layouts.popup',
            'Eventos' => $Eventos
        ]);
    }

    public function buscar(Request $request, $id =0){
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta().'/destruir')) {
            $rs = Eventos::withTrashed()->find($id);
        }else {
            $rs = Eventos::find($id);
        }
        $url = $this->ruta();
        $url=substr($url,0,strlen($url)-7);
        if ($rs) {
            $imgArray=[];
            $imgs = EventosImg::where('eventos_id', $id)->get();

            foreach ($imgs as $img) {
                $id_archivo=str_replace ('/','-', $img->archivo);
                $name=substr($id_archivo, strrpos($id_archivo,'/')+1);
                $imArray[]=[
                    'id'=>$id_archivo,
                    'name'=>$name,
                    'url'=>url('public/archivos/eventos/'.$img->archivo),
                    'thumbnailUrl'=>url('public/archivos/eventos/'.$img->archivo),
                    'deleteType'=>'DELETE',
                    'deleteUrl'=>url($url.'/eliminarimagen'.$id_archivo),
                    'data'=>[
                        'cordenadas'=>[],
                        'leyenda'=>$img->leyenda,
                        'descripcion'=>$img->descripcion
                    ]
                ];
                // dd($imArray);
            }
            $respuesta = array_merge($rs->toArray(),[
                's'=>'s',
                'msj'=>trans('controller.buscar'),
                'files'=>$imArray
            ]);

            return $respuesta;
        }
        return trans('controller.nobuscar');
    }

    public function data($request){
        if ($this->puedePublicar() && $request->input(['published_at']) != '') {

            $data=$request->all();

        }else {
            $data=$request->except(['published_at']);
        }
        // $data['slug'] = str_slug($data['titulo'],'-');
        // if()
        // Carbon::createFromFormat('d/m/Y H:i', $data['published_at']);
        //
        // $archivo = str_replace('-','/',$archivo)

        return $data;
    }

    public function guardar(EventosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try {
            $data = $this->data($request);
            $archivos = json_decode($request->archivos);

            if ($id === 0) {
                $Eventos = Eventos::create($data);
                $id = $Eventos->id;
            }else {
                if (empty($archivos)) {
                    unset($data['published_at']);
                }
                $Eventos = Eventos::find($id)->update($data);
            }

            $this->guardarImagenes($archivos, $id);


            //$this->procesar_permisos($request, $id);
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return ['s' => 's', 'msj' => trans('controller.incluir')];
    }

    protected function getRuta() {
        return date('Y') . '/' . date('m') . '/';
    }

    protected function guardarImagenes($archivos,$id=0){
        foreach ($archivos as $archivo => $data) {
            if (!preg_match("/^(\d{4})\-(\d{2})\-([0-9a-z\.]+)\.(jpe?g|png)$/i", $archivo)) {
                continue;
            }
            $archivo = str_replace('-','/',$archivo);
            $imagen = Image::make(public_path('archivos/eventos/' . $archivo));
            $imagenes=EventosImg::firstOrNew(array('archivo'=>$archivo));
            $imagenes->fill([
                'eventos_id'=>$id,
                'tamano'=>$imagen->width().'x'.$imagen->height(),
                'leyenda'=>$data->leyenda,
                'descripcion'=>$data->descripcion
            ]);
            $imagenes->save();
        }
    }

    public function subir(Request $request) {
        $validator=Validator::make($request->all(),[
            'files.*' => [
                'required',
                'mimes:jpeg,jpg,png'
            ],
        ]);

        if ($validator->fails()) {
            return 'Error de Validación';
        }

        $files = $request->file('files');
        $url = $this->ruta();
        $url = substr($url, 0, strlen($url) - 6);

        $rutaFecha = $this->getRuta();
        $ruta = public_path('archivos/eventos/' . $rutaFecha);

        $respuesta = array(
            'files' => array(),
        );

        foreach ($files as $file) {
            do {
                $nombre_archivo = $this->random_string() . '.' . $file->getClientOriginalExtension();
            } while (is_file($ruta . $nombre_archivo));

            $id = str_replace('/', '-', $rutaFecha . $nombre_archivo);

            $respuesta['files'][] = [
                'id' => $id,
                'name' => $nombre_archivo,
                'size' => $file->getSize(),
                'type' => $file->getMimeType(),
                //'url' => url('imagen/small/noticias/' . $rutaFecha . $nombre_archivo),
                'url' => url('public/archivos/eventos/' . $rutaFecha . $nombre_archivo),
                'thumbnailUrl' => url('public/archivos/eventos/' . $rutaFecha . $nombre_archivo),
                'deleteType' => 'DELETE',
                'deleteUrl' => url($url . '/eliminarimagen/' . $id),
                'data' => [
                    'cordenadas' => [],
                    'leyenda' => '',
                    'descripcion' => ''
                ]
            ];

            $mover = $file->move($ruta, $nombre_archivo);
        }

        return $respuesta;
    }

    protected function random_string($length = 10) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    public function puedePublicar(){
        return strtolower(auth()->user()->super) === 's' || $this->permisologia('publicar');
    }

    public function eliminarImagen(Request $request, $id=0){
        $id = str_replace('-', '/', $id);
		try {
			// \File::delete(public_path('img/noticias/' . $id));
			$rs = EventosImg::where('archivo', $id)->delete();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Eventos::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Eventos::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Eventos::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Eventos::select([
            'id', 'titulo', 'lugar', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}
