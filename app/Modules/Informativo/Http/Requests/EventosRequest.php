<?php

namespace App\Modules\Informativo\Http\Requests;

use App\Http\Requests\Request;

class EventosRequest extends Request {
    protected $reglasArr = [
		'titulo' => ['required', 'min:3', 'max:255'],
		'lugar' => ['required', 'min:3', 'max:255'],
		'slug' => ['required', 'min:3', 'max:255']
	];
}
