<?php

namespace App\Modules\Informativo\Http\Requests;

use App\Http\Requests\Request;

class EfemeridesRequest extends Request {
    protected $reglasArr = [
		'titulo'    => ['required', 'min:3', 'max:255'],
        'slug'      => ['required', 'min:3', 'max:255'],
        //'url'       => ['required'],
        'resumen'   => ['min:3','max:255']
	];
}
