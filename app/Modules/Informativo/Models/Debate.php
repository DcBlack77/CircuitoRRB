<?php

namespace App\Modules\Informativo\Models;

use App\Modules\base\Models\Modelo;
use Carbon\Carbon;



class Debate extends modelo
{
    protected $table = 'debate';
    protected $fillable = ["titulo","slug","contenido","contenido_html","resumen","published_at"];
    protected $campos = [
    'titulo' => [
        'type' => 'text',
        'label' => 'Titulo',
        'placeholder' => 'Titulo del Debate',
        'cont_class' => 'col-md-5'
    ],
    'slug' => [
        'type' => 'text',
        'label' => 'Slug',
        'placeholder' => 'Slug del Debate',
        'cont_class' => 'col-md-5'
    ],
    'published_at' => [
        'type' => 'text',
        'label' => 'Fecha de publicación del debate',
        'placeholder' => 'Fecha del Debate',
        'cont_class' => 'col-md-2'
    ],
    'contenido' => [
        'type' => 'textarea',
        'label' => 'Contenido',
        'placeholder' => 'Contenido del Debate',
        'cont_class' => 'col-md-12'
    ],'contenido_html' => [
        'type' => 'hidden',
        'cont_class' => 'col-md-12'
    ],
    'resumen' => [
        'type' => 'textarea',
        'label' => 'Resumen',
        'placeholder' => 'Resumen del Debate',
        'cont_class' => 'col-md-12'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

    }


}
