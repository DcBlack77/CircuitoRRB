<?php

namespace App\Modules\Informativo\Models;

use App\Modules\base\Models\Modelo;

use App\Modules\Informativo\Models\EventosImg;

use Carbon\Carbon;



class Eventos extends modelo
{
    protected $table = 'eventos';
    protected $fillable = ["titulo","lugar","slug","published_at"];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }
    
    public function imagenes(){
        // belongsTo = "pertenece a" | hace relacion desde el detalle hasta el maestro
        return $this->hasMany('App\Modules\Informativo\Models\EventosImg');
    }


}
