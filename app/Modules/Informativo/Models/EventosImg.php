<?php

namespace App\Modules\Informativo\Models;

use App\Modules\base\Models\Modelo;

use App\Modules\Informativo\Models\Eventos;

class EventosImg extends modelo
{
    protected $table = 'eventos_img';
    protected $fillable = ["eventos_id","archivo","descripcion","leyenda","tamano"];


    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['eventos_id']['options'] = Eventos::pluck('titulo', 'id');
    }

    public function eventos()
    {
        return $this->belongsTo('App\ModulesInformativo\Models\Eventos');
    }

}
