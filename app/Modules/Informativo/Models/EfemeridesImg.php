<?php

namespace App\Modules\Informativo\Models;

use App\Modules\base\Models\Modelo;

use App\Modules\Informativo\Models\Efemerides;

class EfemeridesImg extends modelo
{
    protected $table = 'efemerides_img';
    protected $fillable = ["efemerides_id","archivo","descripcion","leyenda","tamano"];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['efemerides_id']['options'] = Efemerides::pluck('titulo', 'id');
    }

    public function efemerides()
    {
        return $this->belongsTo('App\Modules\Informativo\Models\Efemerides');
    }


}
