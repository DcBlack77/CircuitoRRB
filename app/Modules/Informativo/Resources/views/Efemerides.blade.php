@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')

    @include('base::partials.ubicacion', ['ubicacion' => ['Efemerides']])

    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Efemerides.',
        'columnas' => [
            'Titulo' => '30',
            'Resumen' => '40',
            'Fecha de Publicación' => '30'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        <div class="row col-md-12">
            {!! Form::open(['id'=>'formulario', 'name'=>'formulario', 'method'=>'POST'])!!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info ">
                            <div class="panel-heading">
                                <center>
                                    <h3 class="panel-title">Efemerides</h3>
                                </center>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="titulo" class="requerido">Titulo</label>
                                <input class="form-control" required="required" id="titulo" name="titulo" type="text" v-model="message">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="slug" class="requerido">SLUG</label>
                                <input class="form-control" placeholder="Slug" required="required" id="slug" name="slug" type="text" value="">
                            </div>

                            @if ($controller->puedepublicar())
                            {{ Form::bsText('published_at', '', [
                                'label' => 'Fecha',
                                'placeholder' => 'Fecha de Publicación',
                                'class_cont' => 'col-md-4'
                            ]) }}
                            @endif
                            <div class="form-group col-xs-12">
                                <label for="resumen">Resumen </label>
                                <textarea id="resumen" name="resumen" class="form-control" placeholder="Resumen de la Noticia" required="required"></textarea>
                            </div>
                        </div>
                    </div
                    <div class="profile-sidebar col-md-6" style="float:center; margin-bottom: 0px;">
                        <div class="portlet light profile-sidebar-portlet ">
                            <div class="mt-element-overlay">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mt-overlay-6">
                                            <img id="foto" src="{{ url('public/img/banners/banner.jpg') }}" width="100%"class="img-responsive" alt="">
                                            <div class="mt-overlay">
                                                <h2></h2>
                                                <p>
                                                    <input id="upload" name="foto" type="file" />
                                                    <a href="#" id="upload_link" class="mt-info uppercase btn default btn-outline">
                                                        <i class="fa fa-camera"></i>
                                                    </a>
                                                </p>
                                            </div>
                                            <h4 style="color:#fff;font-weight:bold;">Imagen del Efemerides</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
