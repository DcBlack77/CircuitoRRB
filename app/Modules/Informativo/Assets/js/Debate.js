var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'antes' : function (accion) {
			$("#contenido_html").val(CKEDITOR.instances.contenido.getData());
		},
		'limpiar' : function(){
			tabla.ajax.reload();
			CKEDITOR.instances.contenido.setData('');
		},
		'buscar' : function(r){
			CKEDITOR.instances.contenido.setData(r.contenido_html);
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{data:'titulo',			name:'titulo'},
			{data:'slug',			name:'slug'},
			{data:'resumen',		name:'Resumen'},
			{data:'published_at',	name:'Publicación'}
		]
	});

	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$('#published_at', $form).datetimepicker({
		dateFormat: 'yy-mm-dd '
	});
	var contenido = CKEDITOR.replace('contenido');

});

$(document).ready(function () {
	$("#titulo").keyup(function () {
		var value = slug($("#titulo").val());
		$("#slug").val(value);
	});
});
