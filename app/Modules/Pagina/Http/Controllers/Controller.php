<?php

namespace App\Modules\Pagina\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	// public $app = 'base';
	public $autenticar = false;

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'app/Modules/Pagina/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'app/Modules/Pagina/Assets/css',
	];
	public $libreriasIniciales = [
		'OpenSans',
		'font-awesome',
		'simple-line-icons',
		'jquery-easing',
		'animate',
		'bootstrap',
		'bootbox',
		'owl-carousel',
		'modernizr',
		'wow',
		'lightbox',
		'scroll-top'
	];
	// public $libreriasIniciales = [
	// 	'OpenSans', 'font-awesome', 'simple-line-icons',
	// 	'jquery-easing',
	// 	'animate', 'bootstrap', 'bootbox',
	// 	//'jquery-cookie'
	// 	'pace', 'jquery-form', 'blockUI', 'jquery-shortcuts', 'pnotify', 'owl-carousel', 'modernizr',
	// 	'mixitup','wow','placeholdem','lightbox','scroll-top','lazyload','slicknav'
	// ];
}
