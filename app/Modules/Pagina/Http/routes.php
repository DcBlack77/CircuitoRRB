<?php

Route::group(['middleware' => 'web', 'namespace' => 'App\\Modules\Pagina\Http\Controllers'], function()
{
    Route::get('/',                         'PaginaController@index');
    Route::get('noticias',                  'NoticiasController@index');
    Route::get('enlinea',                   'EnLineaController@index');
    Route::get('noticias/{slug}',           'DetalleNotiController@index');
    Route::get('audio/{slug}',              'AudioDetController@index');
    Route::get('audio',                     'AudioController@index');
    Route::get('galeria',                   'GaleriaController@index');
    Route::get('galeria/{slug}',            'GaleriadetController@index');

    //Route::get('pruebasoundcloud', 'PruebaController@index' );
    //{{route}}
});
