@extends('pagina::layouts.master')
<style type="text/css">
#radio{
    background-position: center center;
}

#myPlayButton img:hover{
    background-color: #00A000;
}
#myStopButton img:hover{
    background-color:#FF5F28;
}
</style>
@section('content')

    <div class="" style="height:300px">

    </div>
    <div class="row">
    	<div class="col s12 m12 l12">
    		<img class="responsive-img" src="{{asset('public/img/radio/vivoBaner.jpg')}}">
    			<div class="black card-panel center-align">
    				<a id="myPlayButton" href="#">
    					<h5  class="yellow-text text-darken-3"> <i class="fa fa-play"></i>&nbsp;Reproducir</h5>
    				</a>
    				<a id="myStopButton" href="#">
    					<h5  class="red-text text-darken-3"> <i class="fa fa-stop"></i>&nbsp;Pausar</h5>
    				</a>
    			</div>
    	</div>

    </div>

    <div id="jquery_jplayer_1" class="jp-jplayer"></div>
    <canvas id="canvas"></canvas>
    @endsection


@section('js')
<script type="text/javascript">
	$(document).on('ready', function(){

		$("#myStopButton").hide();

		$("#myPlayButton").click( function(e) {
			e.preventDefault();
			$("#myStopButton").fadeIn();
			$("#myPlayButton").hide();
		      $("#jquery_jplayer_1").jPlayer("play");
		});

		$("#myStopButton").click( function(e) {
				e.preventDefault();
				$("#myStopButton").hide();
				$("#myPlayButton").fadeIn();
                $("#jquery_jplayer_1").jPlayer("stop");
		});
	})
</script>
@endsection
