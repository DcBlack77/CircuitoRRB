<div id="copyright">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <img src="http://e-bolivar.gob.ve/public/img/Modules/Pagina/logo.png" class="img-responsive" style="margin: auto;">
            </div>
            <div class="col-xs-12 text-center">
                <div class="row ">
                    <div class="col-xs-12 sociales">
                        <span>
                            <a href="https://www.facebook.com/gobiernodebolivar/" target="_blank" class="redes"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/PrensaGoBol/" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.instagram.com/prensagobol/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="https://www.youtube.com/channel/UCbb9IJF38A3cxZKds_PDayQ" target="_blank"><i class="fa fa-youtube-play"></i></a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-12 text-center">
                <img src="http://e-bolivar.gob.ve/public/img/Modules/Pagina/fondonorma.png">
            </div>
            <div class="col-md-8 col-sm-12">
                <span> 
                    <a href="#" data-toggle="modal" data-target="#institucional" style="text-decoration: none;color:#fff;">Institucional</a> | 
                </span>
                <span> 
                    <a href="#" data-toggle="modal" data-target="#mapa_sitio" style="text-decoration: none;color:#fff;">Mapa de sitio</a>
                </span>
                <span> 
                    | <a href="#" data-toggle="modal" data-target="#contactanos" style="text-decoration: none;color:#fff;">Contáctanos</a> 
                </span>
                <br><br>
                Diseñado y desarrollado por la <span style="color:#FEC72E">Dirección de Informática y Sistemas</span> Todos los derechos reservados
            </div>
            <div class="col-md-2 col-sm-12 text-center">
                <img src="http://e-bolivar.gob.ve/public/img/Modules/Pagina/gestion.png">
            </div>
        </div>
    </div>
</div>



footer {
    background: #F5F7FA;
    color: #ecf0f1;
}


footer #copyright {
    background: #1A1B1D;
    text-align: center;
    font-size: 1em;
    font-weight: bold;
    padding: 20px 0;
}