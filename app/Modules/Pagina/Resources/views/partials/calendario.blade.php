@section('css')
        <rel="stylesheet" type="text/css" href="{{asset('plugins/fullcalendar/css/fullcalendar.min.css')}}">
        <rel="stylesheet" type="text/css" href="{{asset('plugins/fullcalendar/css/fullcalendar.print.css')}}" media="print">
        <rel="stylesheet" href="{{asset('plugins/fullcalendar/css/cupertino/jquery-ui.min.css')}}">
@endsection

@section('contenido')
<div class="row">
<div class="card">
<div class="col l7 offset-l1 m12 s12">
<div id="calendar"></div>
</div>
</div>
</div>
@endsection
@section('scripts')
    <script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{asset('plugins/fullcalendar/js/moment.min.js')}}"></script>
    <script src="{{asset('plugins/fullcalendar/js/fullcalendar.js')}}"></script>
    <script src="{{asset('plugins/fullcalendar/js/lang-all.js')}}"></script>
    <script>
        //inicializamos el calendario al cargar la pagina
        $(document).ready(function() {

            $('#calendar').fullCalendar({

                header: {
                    left: 'prev,next today myCustomButton',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },

            });

        });
    </script>
