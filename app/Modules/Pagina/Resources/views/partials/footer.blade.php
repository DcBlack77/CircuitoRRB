<script type="text/javascript">
  var $url = "{{ URL::current() }}/",
  sessionLife = {{ \Config::get('session.lifetime') }};
</script>

<!-- requried-jsfiles-for owl -->

<!-- //here ends scrolling icon -->
<!--js for bootstrap working-->


@if (isset($html['js']))
@foreach ($html['js'] as $js)
<script type="text/javascript" src="{{ url($js) }}?v={{ env('APP_VERSION') }}"></script>
@endforeach
@endif

<!-- //requried-jsfiles-for owl -->
<!-- start-smoth-scrolling -->

@stack('js')
