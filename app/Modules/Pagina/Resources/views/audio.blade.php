@extends('pagina::layouts.master')

@section('content')

{{-- Audios --}}
<div class="row">
    <div class="audios" id="audio">
        <div style="height:80px;"></div>
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <h2 class="titulo">Audios</h2>
            <div class="titulo"style="height:30px;">
                <input style="color:black; text-align:center;" class="" type="text" id="search" placeholder="Buscar Audio" autofocus />
            </div>
            <div class="container">
                @foreach ($audios as $key => $audio)
                    <div class="contenedor">
                        <div class="col-md-offset-2 col-md-8">
                            <div class="row">
                                <a href="{{url('audio/'.$audio->slug)}}">
                                    <div class="noticias card ho">
                                        <div class="col-md-12">
                                            <h3 class=" titulo-noticia">{!! $audio->titulo !!}</h3>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="" style="margin-top:1em;">
                                                <center>
                                                    <audio style="width:80%;"  src="{{url('public/audio/'. $audio->archivo)}}" controls="controls" preload="">

                                                    </audio>
                                                </center>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="margin-top:1em;">
                                            <div class="texto-resumen" style="text-align:justify">
                                                <center><p>{!! str_replace("\n", '<br/>', $controller->limit_text($audio->descripcion,90)) !!}</p></center>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="" style="height:65px; background-color: #000"></div>



@endsection

@section('js')

@endsection
