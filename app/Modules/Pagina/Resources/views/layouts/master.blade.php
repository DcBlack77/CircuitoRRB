<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        @include('pagina::partials.head')
    </head>
    <body>
        @include('pagina::partials.header-index')
        <div class="row">
            <div class="contendor-body">
                @yield('content-top')
                @yield('content')

                @include('pagina::partials.footer-page')
                @include('pagina::partials.footer')
            </div>

        </div>


    </body>
</html>
