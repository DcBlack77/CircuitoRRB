@extends('pagina::layouts.master')
@section('content')

    {{--NOTICIAS--}}
    <div class="row">
        <div class="unoticias" id="unoticias">
            <div style="height:80px;"></div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <h2 class="titulo">Noticias</h2>
                <div class="titulo"style="height:30px;">
                    <input style="color:black; text-align:center;" class="" type="text" id="search" placeholder="Buscar Noticia" autofocus />
                </div>
                <div class="row">
                    <?php $i=1; ?>
                    @foreach ($noticias->get() as $noticia)
                        @if ($i == 1 )
                        @endif
                        <div class="contenedor">
                            <div class="col-md-offset-1 col-md-10">
                                <div class="row">
                                    <a href="{{url('noticias/'.$noticia->slug)}}">
                                        <div class="noticias card ho">
                                            <div class="col-md-4 ">
                                                <div class="">
                                                    <img class="img-thumbnail" width="200px" height="200px" src="{{ url('public/archivos/noticias/'.$noticia->imagenes[0]->archivo) }}" alt="{{$noticia->imagenes[0]->leyenda}}">
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <h3 class=" titulo-noticia">{!! $noticia->titulo !!}</h3>

                                            </div>
                                            <div class="col-md-12">
                                                <div class="texto-resumen" style="text-align:justify">
                                                    <p>{!! str_replace("\n", '<br/>', $controller->limit_text($noticia->contenido,90)) !!}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-4 col-sm-3 col-xs-3 col-lg-4">
                                                    <p class="fecha-dia">{{ str_pad($noticia->published_at->day, 2, '0', STR_PAD_LEFT) }}</p>
                                                </div>
                                                <div class="col-md-4 col-sm-3 col-xs-3 col-lg-4">
                                                    <p class="fecha-mes"> {{ $controller->meses[$noticia->published_at->month] }}</p>
                                                </div>
                                                <div class="col-md-4 col-sm-3 col-xs-3 col-lg-4">
                                                    <p class="fecha-ano"> {{ $noticia->published_at->year }}</p>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 noti-top"></div>
                                                <div class="col-md-1"></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
