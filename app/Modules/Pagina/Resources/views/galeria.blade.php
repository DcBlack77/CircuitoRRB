@extends('pagina::layouts.master')

@section('content')

{{-- Audios --}}
<div class="row">
    <div class="galeria" id="galeria">
        <div style="height:80px;"></div>
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <h2 class="titulo">Galerias</h2>
            <div class="titulo"style="height:30px;">
                <input style="color:black; text-align:center;" class="" type="text" id="search" placeholder="Buscar Galeria" autofocus />
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <div class="container">
                @foreach ($galerias as $galeria)
                    <div class="row">
                        <a href="{{url('galeria/'.$galeria->slug)}}">
                            <div class="">
                                <h2 class="titulo" style="margin-top:1.5em; color:white;">{{ $galeria->nombre }}</h2>
                            </div>
                        </a>
                        @foreach ($galeria->imagenes->take(6) as $imagene)
                            <div style="margin-top:1.5em;" class="col-md-2 col-sm-3 col-xs-4">
                                <img class="img img-responsive img-thumbnail" height="170px" width="170px" src="{{ url('public/archivos/galeria/' . $imagene->archivo) }}" alt="{{ $imagene->leyenda }}" width='100%' />
                            </div>
                        @endforeach
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="" style="height:65px; background-color: #000"></div>



@endsection

@section('js')

@endsection
