@extends('pagina::layouts.master')
<link rel="stylesheet" type="text/css" href="{{ url('public/css/prettyPhoto.css') }}">
@section('content')
    <div style="height:80px;"></div>
<div class="col-md-12 col-sm-10 col-xs-10 col-lg-12">
    <?php $i=1; ?>
    @foreach ($galerias as $galeria)
        @if ($i == 1 )
        @endif
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h3 class=" titulo-noticia">{!! $galeria->titulo !!}</h3>
            <div class="col-xs-12 col-md-12" style="scroll-behavior: smooth; margin-top: 75px;">
                <div class="panel panel-warning" style="border-radius: 0px;">
                    <div class="panel-heading" style="border-radius: 0px;">
                        <h3 class="panel-title text-center"> Galeria Fotogr&aacute;fica</h3>
                    </div>
                    <div class="panel-body" style="background-color: rgb(0, 0, 0);">
                        <ul class="gallery clearfix" style="list-style:none">
                            <?php $num = 0;?>
                                @foreach ($galeria->imagenes as $imagene)
                                    <li style="margin: 0px 1px 4px 0; list-style:none">
                                        <div class="col-md-4">
                                            <a href="{{ url('public/archivos/galeria/' . $imagene->archivo) }}" data-lightbox="roadtrip" title="{{$imagene->leyenda}}">
                                                <img class="img-responsive" style="width:100%" src="{{ url('public/archivos/galeria/' . $imagene->archivo) }}" alt="{{$imagene->leyenda}}">
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

@endsection


@section('js')
    <script>
        lightbox.option({
          'resizeDuration': 200,
          'wrapAround': true
        })
    </script>
@endsection
