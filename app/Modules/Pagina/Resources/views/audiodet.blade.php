@extends('pagina::layouts.master')

@section('content')
    <div style="height:80px;"></div>
<div class="col-md-12 col-sm-10 col-xs-10 col-lg-12">
    <?php $i=1; ?>
    @foreach ($audios as $audio)
        @if ($i == 1 )
        @endif
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h3 class=" titulo-noticia">{!! $audio->titulo !!}</h3>
            <div class="row">
                <div class="noticias card">
                    <div class="row">
                        <div class="col-md-12 col-sm-10 col-xs-10 col-lg-12 noti-top"></div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="" style="margin-top:1em;">
                            <center>
                                <audio style="width:80%;"  src="{{url('public/audio/'. $audio->archivo)}}" controls="controls" preload="">

                                </audio>
                            </center>
                        </div>
                    </div>
                    <div class="texto-resumen" style="text-align:justify">
                        <p style="margin-left:5px;margin-right:5px;">{!! $audio->descripcion !!}</p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

@endsection


@section('js')

@endsection
