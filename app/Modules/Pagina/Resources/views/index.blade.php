@extends('pagina::layouts.master')



@section('content')
<div class="" style="height:65px; background-color: #000"></div>
<!-- Carousel principal -->
<div id="carousel" class="owl-carousel hidden-xs">
	@foreach($banners as $banner)
		<div class="item">
			<img src="{{ url('public/img/banners/'. $banner['archivo'] ) }}">
		</div>
	@endforeach
    @php
        //dd($banner);
    @endphp
	@for($i=1; $i <= 7 ; $i++)
	@endfor
</div>

<!-- NOTICIAS -->
<div class="row">
	<div class="unoticias" id="unoticias">
		<div class="col-md-12 col-sm-10 co-xs-10 col-lg-12">
			<h2 class="titulo" style="padding-bottom:2em;">NOTICIAS</h2>
			<div class="col-md-4 col-sm-8 col-xs-8 col-lg-4">
				<div class="row twitter">
					<div class="blog-item">
						<div class="blog-item-wrapper wow fadeIn" data-wow-delay="0.3s" >
							<div class="blog-item-img" style="margin-left:60px;">
								<a class="twitter-timeline" data-width="550" data-height="1180" data-theme="dark" data-link-color="#FAB81E" href="https://twitter.com/RRB1011FM">Tweets by RRB1011FM</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-8 col-sm-8 col-xs-8 col-lg-8">
				<h2>Últimas Noticias</h2>
				<div class="row">
					<div class="noticias">
						<div class="col-md-12 col-sm-8 col-xs-8 col-lg-12">
							<?php $i=1; ?>
							@foreach ($noticias->get() as $key => $noticia)
								@if ($i == 1 )
								@endif
								<article class="articulo-noticias card">
									<div class="row">
                                        <div class="col-md-3 col-sm-8 col-xs-8 col-lg-3" style="margin-left:40px;">
                                            <a class="" href="{{ url('noticias/'.$noticia->slug) }}">
                                                <img class="img img-responsive img-thumbnail" height="250px" width="250px" src="{{ url('public/archivos/noticias/'.$noticia->imagenes[0]->archivo) }}" alt="{{ $noticia->imagenes[0]->leyenda }}" height="100%">
                                            </a>
                                        </div>
                                        <div class=" col-md-1 col-sm-1 col-xs-1 col-lg-1" style="margin-left: 10px;">
                                            <div class="col-md-12 col-sm-1 col-xs-1 col-lg-12">
                                                <p class="fecha-dia">{{ str_pad($noticia->published_at->day, 2, '0', STR_PAD_LEFT) }}</p>
                                            </div>
                                            <div class="col-md-12 col-sm-1 col-xs-1 col-lg-12">
                                                <p class="fecha-mes"> {{ $controller->meses[$noticia->published_at->month] }}</p>
                                            </div>
                                            <div class="col-md-12 col-sm-1 col-xs-1 col-lg-12">
                                                <p class="fecha-ano">{{ $noticia->published_at->year }}</p>
                                            </div>
                                            <div class="col-md-12 col-sm-10 col-xs-10 col-lg-12 noti-top"></div>
                                        </div>
                                        <div class="col-md-7 col-sm-8 col-xs-8 col-lg-7" style="margin-top: -40px; width: 420px; margin-left: 20px;">
                                            <div class="">
                                                <h3 class="titulo">{{ $noticia->titulo }}</h3>
                                            </div>
                                            <div class="contenido">
                                                <p style="font-size:14px;">{!! str_replace("\n", '<br/>', $controller->limit_text($noticia->resumen,60)) !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="margin-bottom:25px;">
                <a href="noticias" target="_blank">
                    <h4 class="titulo"> Más Noticias</h4>
                </a>
            </div>
        </div>
    </div>
</div>



<!-- DEBATE -->
<div class="row" style="z-index:-1;">
    @if ($debates == true)
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <section id="debate" class="">
                <div class="row">
                    <img class="img img-responsive" src="{{ url('public/archivos/debate/debate.jpg') }}" data-original="" style="width: 100%; height: auto; display: inline; margin-top:0 auto;">
                </div>
                {{-- <div class="debate_texto">
                    <h2>{!! $debates->titulo !!}</h2>
                    <p class="text-justify" style="margin-top:15px;">
                        {!! $debates->resumen !!}
                    </p>
                    <div class="firma">
                        Gob. Francisco Rangel Gómez<br> @RangelGomez | #Debate
                    </div> --}}
                </div>
            </section>
        </div>
    @endif
</div>

<!-- PROGRAMAS -->
<div class="row" style="z-index:100;">
    <div id="programas" class="programas">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <div class="tabla-programas" >
                @include('pagina::partials.programas')
            </div>
        </div>
    </div>
</div>

<!-- EVENTOS -->
<div class="row">
    <div id="eventos" class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
        <div class="row">
            <div class="titulo">
				<h2 style="color:white; margin-top:1.5em;">Eventos</h2>
			</div>
            @foreach ($eventos as $evento)
                <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
                    @foreach ($evento->imagenes->take(1) as $imagene)
                        <div class="">
                            <img class="img img-responsive" src="{{ url('public/archivos/eventos/' .$imagene->archivo) }}" alt="{{ $imagene->leyenda }}" width="100%">
                        </div>
                    @endforeach
                    <div class="carousel-caption">
                        <h2>{{$evento->lugar}}</h2>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<!-- EFEMERIDES -->
<div class="row" id="efemerides">
    <div class="container">
        <div class="titulo">
            <h2 style="color:white; margin-top:1.5em;">Efemérides</h2>
        </div>
        <div class="row">
            @foreach ($efemerides as $key => $efemeride)
                <div class="col-md-3 col-sm-12">
                    <div class="planes popup-gallery">
                        <a href="{{ url('public/archivos/efemerides/'.$efemeride['url']) }}" title="{{ $efemeride->titulo }}">
                            <img class="img img-responsive" alt="{{ $efemeride->resumen }}" src="{{ url('public/archivos/efemerides/'.$efemeride['url']) }}" data-original="{{ url('public/archivos/efemerides/'.$efemeride['url']) }}" style="min-width: 263px; max-height: 262px !important; display: block; height: auto;">
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<!-- GALERIA -->
<div class="row">
    <div id="galeria" class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
        <div class="container">
        @foreach ($galerias as $galeria)
			<div class="row">
				<a href="{{url('galeria/'.$galeria->slug)}}">
					<div class="">
	                    <h2 class="titulo" style="margin-top:1.5em; color:white;">{{ $galeria->nombre }}</h2>
	                </div>
	                @foreach ($galeria->imagenes->take(6) as $imagene)
	                    <div class="col-md-2 col-sm-3 col-xs-4">
	                        <img class="img img-responsive img-thumbnail" height="170px" width="170px" src="{{ url('public/archivos/galeria/' . $imagene->archivo) }}" alt="{{ $imagene->leyenda }}" width='100%' />
	                    </div>
	                @endforeach
				</a>
            </div>
        @endforeach
        </div>
    </div>
</div>

<!-- AUDIOS -->
{{-- <div class="row">
    <div id="audio"class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
        <div class="container">
            @foreach ($audios->take(4) as $audio)
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <audio controls>
                            <source src="{{url('public/archivos/audio/' . $audi->url ) }}" type="audio">
                        </audio>
                    </div>
                    <div class="">
                        <h2 class="titulo" style="margin-top:1.5em; color:white;">{{ $audio->titulo }}</h2>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div> --}}

<!-- VIDEOTECA -->
<div class="row">
    <div id="videos" class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
        <div id="videoteca">
            <div class="titulo">
                <h2 style="color:white;margin-top:1.5em;">Vídeos</h2>
            </div>
            <div class="row">
                @foreach ($videos->get() as $video)
                    <div class="col-md-5 col-sm-12 col-xs-12 col-lg-5" style="margin-left:auto;margin-right: 85px; margin-bottom:25px;">
                        <div class="videoscs">
                            {!! $video->get_iframe() !!}
                            <div class="carousel-caption">
                                <h2>{{$video->titulo}}</h2>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="margin-bottom:25px;">
                <a href="https://www.youtube.com/channel/UCbb9IJF38A3cxZKds_PDayQ/videos" target="_blank">
                    <h4 class="titulo"> Más Vídeos</h4>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- ORGANIZACION -->
<div class="row" id="organizacion">
    @if ($organizaciones == true)
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <div class="resumen">
                    <div class="col-md-offset-2 col-md-8">
                        <div class="titulo">
                            <h2>Reseña Histórica</h2>
                        </div>
                        <div class="texto">
                            {!! $organizaciones->objetivo !!}
                        </div>
                    </div>
                </div>
                <div class="resumen">
                    <div class="col-md-offset-1 col-md-4">
                        <div class="titulo">
                            <h2>Misión</h2>
                        </div>
                        <div class="texto">
                            {!! $organizaciones->mision !!}
                        </div>
                    </div>
                </div>
                <div class="resumen">
                    <div class="col-md-offset-2 col-md-4">
                        <div class="titulo">
                            <h2>Visión</h2>
                        </div>
                        <div class="texto">
                            {!! $organizaciones->vision !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>


<!-- Realizado por la dirección de Informatica y Sistemas de la Gobernación del Estado Bolívar -->


<!-- Creditos: -->

<!-- Diseño: Maria Campora "camporamaria@gmail.com" -->

<!-- Desarrollador: Alejandro Mendez "alejmendez.87@gmail.com" -->

<!-- Desarrollador: Diego Cabrera "diegocabrera123@gmail.com" -->

@stop
