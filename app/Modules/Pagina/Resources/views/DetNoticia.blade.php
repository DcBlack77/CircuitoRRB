@extends('pagina::layouts.master')
<link rel="stylesheet" type="text/css" href="{{ url('public/css/prettyPhoto.css') }}">
@section('content')
{{--NOTICIAS--}}
<div class="row">
    <div class="unoticias" id="unoticias">
        <div style="height:80px;"></div>
        <div class="col-md-12 col-sm-10 col-xs-10 col-lg-12">
            <div class="">
                <?php $i=1; ?>
                @foreach ($noticias->get() as $noticia)
                    <?php
                    $imagen = clone $imagenes;
                    $imagen = $imagen->where('noticias_id', $noticia->id)->get();
                    if (!$imagen) {
                        $imagen = new stdClass();
                        $imagen->leyenda = '';
                        $imagen->archivo = 'gob.png';
                    }
                    ?>
                    @if ($i == 1 )
                    @endif
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <h3 class=" titulo-noticia">{!! $noticia->titulo !!}</h3>
                        <div class="row">
                            <div class="noticias card">
                                <div class="row">
                                    <div class="col-md-12 col-sm-10 col-xs-10 col-lg-12 noti-top"></div>
                                    <div class="col-md-1"></div>
                                </div>
                                <div class="texto-resumen" style="text-align:justify">
                                    <p style="margin-left:5px;margin-right:5px;">{!! $noticia->contenido_html !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4" style="scroll-behavior: smooth; margin-top: 75px;">
                        <div class="panel panel-warning" style="border-radius: 0px;">
                            <div class="panel-heading" style="border-radius: 0px;">
                                <h3 class="panel-title text-center"> Galeria Fotogr&aacute;fica</h3>
                            </div>
                            <div class="panel-body" style="background-color: rgb(0, 0, 0);">
                                <ul class="gallery clearfix" style="list-style:none">
                                    <?php $num = 0;?>
                                    @foreach ($imagen as $key => $image)
                                        <li style="margin: 0px 1px 4px 0; list-style:none">
                                            <a href="{{ url('public/archivos/noticias/'.$image->archivo) }}" data-lightbox="roadtrip" title="{{$noticia->titulo}}">
                                                <img class="img-responsive" style="width:100%" src="{{ url('public/archivos/noticias/'.$image->archivo) }}" alt="{{$image->leyenda}}">
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @if ($noticia->audio === true)
                        <div class="col-xs-12">
                            <audio style="width:80%;" src="{{url('public/archivos/noticias/audio/'.$noticia->audio)}}" controls autoplay loop>

                            </audio>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection
@push('js')
    <script>
        lightbox.option({
          'resizeDuration': 200,
          'wrapAround': true
        })
    </script>
@endpush
