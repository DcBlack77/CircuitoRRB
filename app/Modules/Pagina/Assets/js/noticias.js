var search = document.getElementById("search"),
    food = document.getElementsByTagName("h3"),
    forEach = Array.prototype.forEach;

search.addEventListener("keyup", function(e){
    var choice = this.value;

    forEach.call(food, function(f){
        if (f.innerHTML.toLowerCase().search(choice.toLowerCase()) == -1)
            f.parentNode.style.display = "none";
        else
            f.parentNode.style.display = "inline-block";
    });
}, false);

var sliderInterval, sliders, slider_i;
$(function(){
	$("#carousel").owlCarousel({
		animateOut: 'fadeOut',
		autoplay:true,
		autoplayTimeout:4000,
		autoplayHoverPause:true,
		singleItem : true,
		slideSpeed : 400,
		items:1,
		responsiveRefreshRate : 200,
		responsiveClass:true,
		dots: false,
		nav: false,
		lazyLoad:true,
    	loop:true,
		navText: [],
		onChanged: function(ev){
			console.log(ev);
		}
	});
});
