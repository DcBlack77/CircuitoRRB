<?php 

namespace Modules\Pagina\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;


abstract class Controller extends BaseController {
	public $autenticar = false;

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Pagina/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Pagina/Assets/css',
	];

	public $libreriasIniciales = [
		'OpenSans', 'font-awesome', 'simple-line-icons',
		'jquery-easing', 'jquery-migrate', 
		'animate', 'bootstrap', 'bootbox',
		'jquery-cookie',
		'pace', 'jquery-form', 'blockUI', 'jquery-shortcuts', 'pnotify',
		'init',

		//adicionales de la pagina
		'owl-carousel', 'magnific-popup','mediaelement','revolution-slider',
		/*pagina web*/
		'mixitup','wow','placeholdem','vide','prettyPhoto','waypoints','counterup','scroll-top','lazyload','appear','slicknav',

	];
}




