<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Galeria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galeria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 200);
            $table->timestamp('published_at')->nullable();
            $table->string('slug', 250)->unique();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('galeria_imagenes', function (Blueprint $table) {
            
			$table->increments('id');
			$table->integer('galeria_id')->unsigned();
			$table->string('archivo', 200);
			$table->string('descripcion', 200);
			$table->string('leyenda', 200);
			$table->string('tamano', 12);

			$table->foreign('galeria_id')
				->references('id')->on('galeria')
				->onDelete('cascade')->onUpdate('cascade');

			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('galeria_imagenes');
        Schema::drop('galeria');
    }
}
