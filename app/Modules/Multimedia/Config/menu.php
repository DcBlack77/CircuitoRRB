<?php
    $menu['multimedia']= [
        [
            'nombre' => 'Multimedia',
            'direccion' => '#Multimedia',
            'icono' => 'fa fa-eye',
            'menu' => [
                [
                    'nombre' => 'Galeria',
                    'direccion' => 'multimedia/galeria',
                    'icono' => 'fa fa-picture-o'
                ],
                [
                    'nombre' => 'Audio',
                    'direccion' => 'multimedia/audio',
                    'icono' => 'fa fa-volume-up '
                ],
                [
                    'nombre' => 'Video',
                    'direccion' => 'multimedia/video',
                    'icono' => 'fa fa-video-camera'
                ]
            ]
        ]
    ];
 ?>
