<?php

namespace App\Modules\Multimedia\Models;

use App\Modules\base\Models\Modelo;



class Video extends modelo
{
    protected $table = 'video';
    protected $fillable = [
        "titulo",
        "url",
        "descripcion"
    ];

    protected $campos = [
        'titulo' => [
            'type' => 'text',
            'label' => 'Titulo',
            'placeholder' => 'Titulo del Video'
        ],
        'url' => [
            'type' => 'text',
            'label' => 'Url',
            'placeholder' => 'Url del Video'
        ],
        'descripcion' => [
            'type' => 'textarea',
            'label' => 'Descripcion',
            'placeholder' => 'Descripcion del Video',
            'cont_class'  => 'col-sm-12'
        ]
    ];

    public function get_code()
    {
        $video_id = $this->url;

        if (preg_match('/(?:youtube(?:-nocookie)?\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i', $this->url, $match)) {
            $video_id = $match[1];
        }

        return $video_id;
    }

    public function get_url_embed()
    {
        return 'https://www.youtube.com/embed/' . $this->get_code();
    }

    public function get_iframe()
    {
        return '<iframe width="100%" height="320" src="' . $this->get_url_embed() . '" frameborder="0" allowfullscreen></iframe>';
    }
}
