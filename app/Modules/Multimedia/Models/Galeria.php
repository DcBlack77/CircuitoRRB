<?php

namespace App\Modules\Multimedia\Models;

use App\Modules\Base\Models\Modelo;
use Carbon\Carbon;


class Galeria extends modelo
{
    protected $table = 'galeria';
    protected $fillable = ['id', 'nombre', 'slug', 'published_at'];

    public function imagenes()
	{
        return $this->hasMany('App\Modules\Multimedia\Models\GaleriaImagenes');
    }
}
