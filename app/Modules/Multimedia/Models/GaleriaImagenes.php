<?php

namespace App\Modules\Multimedia\Models;

use App\Modules\base\Models\Modelo;


class GaleriaImagenes extends modelo
{
    protected $table = 'galeria_imagenes';
    protected $fillable = ["galeria_id","archivo","descripcion","leyenda","tamano"];
    protected $campos = [
        'archivo' => [
            'type' => 'text',
            'label' => 'Archivo',
            'placeholder' => 'Archivo del Galeria Imagenes'
        ],
        'descripcion' => [
            'type' => 'text',
            'label' => 'Descripcion',
            'placeholder' => 'Descripcion del Galeria Imagenes'
        ],
        'leyenda' => [
            'type' => 'text',
            'label' => 'Leyenda',
            'placeholder' => 'Leyenda del Galeria Imagenes'
        ],
        'tamano' => [
            'type' => 'text',
            'label' => 'Tamano',
            'placeholder' => 'Tamano del Galeria Imagenes'
        ]
    ];

    public function galeria()
	{
        return $this->belongsTo('App\Modules\Multimedia\Models\Galeria');
    }
}
