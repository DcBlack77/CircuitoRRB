<?php

namespace Modules\Multimedia\Http\Requests;

use App\Http\Requests\Request;

class AudioRequest extends Request {
    protected $reglasArr = [
		'titulo' => ['required', 'min:3', 'max:200'], 
		'url' => ['required', 'min:3', 'max:200'], 
		'descripcion' => ['required']
	];
}