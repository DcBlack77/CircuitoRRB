<?php

namespace Modules\Multimedia\Http\Requests;

use App\Http\Requests\Request;

class VideoRequest extends Request {
    protected $reglasArr = [
		'titulo' => ['required', 'min:3', 'max:200'], 
		'url' => [
			'required', 
			'regex:/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/', 
			'max:200'
		], 
		'descripcion' => ['required']
	];
}