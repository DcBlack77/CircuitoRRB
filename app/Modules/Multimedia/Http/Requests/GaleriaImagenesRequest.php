<?php

namespace App\Modules\Multimedia\Http\Requests;

use App\Http\Requests\Request;

class GaleriaImagenesRequest extends Request {
    protected $reglasArr = [
		'archivo' => ['required', 'min:3', 'max:200'], 
		'descripcion' => ['required', 'min:3', 'max:200'], 
		'leyenda' => ['required', 'min:3', 'max:200'], 
		'tamano' => ['required', 'min:3', 'max:12']
	];
}