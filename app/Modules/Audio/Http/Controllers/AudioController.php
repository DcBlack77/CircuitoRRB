<?php

namespace App\Modules\Audio\Http\Controllers;

//Controlador Padre
use App\Modules\Audio\Http\Controllers\Controller;

//Dependencias
use DB;
use Validator;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Audio\Http\Requests\AudioRequest;

//Modelos
use App\Modules\Audio\Models\Audio;
use App\Modules\Noticias\Models\Categorias;

class AudioController extends Controller
{
    protected $titulo = 'Audio';

    public $js = [
        'Audio'
    ];

    public $css = [
        'Audio'
    ];

    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker',
        'file-upload',
        'jcrop',
    ];

    public function index()
    {
        return $this->view('audio::Audio', [
            'Audio' => new Audio()
        ]);
    }

    public function nuevo()
    {
        $Audio = new Audio();
        return $this->view('audio::Audio', [
            'layouts' => 'base::layouts.popup',
            'Audio' => $Audio
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Audio = Audio::find($id);
        return $this->view('audio::Audio', [
            'layouts' => 'base::layouts.popup',
            'Audio' => $Audio
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Audio = Audio::withTrashed()->find($id);
        } else {
            $Audio = Audio::find($id);
        }

        if ($Audio){
            $url = $this->ruta();
            $url = substr($url, 0, strlen($url) - 7);
            $imgArray = [];

                $id_archivo = str_replace('/', '-', $Audio->archivo);
                $name = substr($id_archivo, strrpos($id_archivo, '/') + 1);
                $imgArray[] = [
                    'id' => $id_archivo,
                    'name' => $name,
                    'url' => url('public/audio/audio.png'),
                    'thumbnailUrl' =>url('public/audio/audio.png'),
                    'deleteType' => 'DELETE',
                    'deleteUrl' => url($url . '/eliminaraudio/' . $id_archivo),
                    'data' => [
                        'cordenadas' => [],
                        'leyenda' => '',
                        'descripcion' => ''
                    ]
                ];
            return array_merge($Audio->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar'),
                'files' => $imgArray,
            ]);
        }

        return trans('controller.nobuscar');
    }

    protected function data($request) {
        $data = $request->all();
        $archivos = json_decode($request->archivo);
        foreach ($archivos as $archivo => $dato) {
            if (!preg_match("/^(\d{4})\-(\d{2})\-([0-9a-z\.]+)\.(mp3|wma|ogg)$/i", $archivo)) {
                continue;
            }
            $archivo = str_replace('-', '/', $archivo);
        }
        $data ['archivo'] = $archivo;
        return $data;
    }

    public function guardar(AudioRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{

            $data = $this->data($request);

            $Audio = $id == 0 ? new Audio() : Audio::find($id);

            $Audio->fill($data);
            $Audio->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Audio->id,
            'texto' => $Audio->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Audio::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Audio::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Audio::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Audio::select([
            'id',
            'archivo',
            'titulo',
            'descripcion',
            'activo',
            'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->editColumn('archivo', '<audio width="100" height="32" src="{{ url(\'public/audio/\'.$archivo)}}" controls="controls" preload=""></audio>')
            ->make(true);
    }

    public function categoria(){
        return  Categorias::pluck('nombre', 'id');
    }

    public function subir(Request $request) {

        $files = $request->file('files');
        $url = $this->ruta();
        $url = substr($url, 0, strlen($url) - 6);

        $rutaFecha = $this->getRuta();
        $ruta = public_path('audio/' . $rutaFecha);

        $respuesta = array(
            'files' => array(),
        );

        foreach ($files as $file) {
            do {
                $nombre_archivo = $this->random_string() . '.' . $file->getClientOriginalExtension();
            } while (is_file($ruta . $nombre_archivo));

            $id = str_replace('/', '-', $rutaFecha . $nombre_archivo);

            $respuesta['files'][] = [
                'id' => $id,
                'name' => $nombre_archivo,
                'size' => $file->getSize(),
                'type' => $file->getMimeType(),
                //'url' => url('imagen/small/' . $rutaFecha . $nombre_archivo),
                'url' => url('public/audio/audio.png'),
                'thumbnailUrl' =>url('public/audio/audio.png'),
                'deleteType' => 'DELETE',
                'deleteUrl' => url($url . '/eliminaraudio/' . $id),
                'data' => [
                    'cordenadas' => [],
                    'titulo' => '',
                    'descripcion' => ''
                ]
            ];

            $mover = $file->move($ruta, $nombre_archivo);
        }

        return $respuesta;
    }

    protected function getRuta() {
        return date('Y') . '/' . date('m') . '/';
    }

    protected function random_string($length = 20) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }
}
